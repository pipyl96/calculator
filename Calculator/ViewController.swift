//
//  ViewController.swift
//  Calculator
//
//  Created by unitech on 9/15/20.
//  Copyright © 2020 unitech. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet weak var resultLabel: UILabel!
    var result: String = "" {
        didSet {
            if result.count > 1, let firstCharactor = result.first, firstCharactor == "0" {
                result.remove(at: result.startIndex)
            }
            resultLabel.text = result
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func didTap0Button(_ sender: Any) {
        result.append("0")
    }
    @IBAction func didTap1Button(_ sender: Any) {
        result.append("1")
    }
    @IBAction func didTap2Button(_ sender: Any) {
        result.append("2")
    }
    @IBAction func didTap3Button(_ sender: Any) {
        result.append("3")
    }
    @IBAction func didTap4Button(_ sender: Any) {
        result.append("4")
    }
    @IBAction func didTap5Button(_ sender: Any) {
        result.append("5")
    }
    @IBAction func didTap6Button(_ sender: Any) {
        result.append("6")
    }
    @IBAction func didTap7Button(_ sender: Any) {
        result.append("7")
    }
    @IBAction func didTap8Button(_ sender: Any) {
        result.append("8")
    }
    @IBAction func didTap9Button(_ sender: Any) {
        result.append("9")
    }
    @IBAction func didTapPlusButton(_ sender: Any) {
    }
    @IBAction func didTapMinusButton(_ sender: Any) {
    }
    @IBAction func didTapEqualButton(_ sender: Any) {
    }
    @IBAction func didTapMultiplyButton(_ sender: Any) {
    }
    @IBAction func didTapDivisorButton(_ sender: Any) {
    }
    @IBAction func didTapDeleteButton(_ sender: Any) {
        result = "0"
    }
}

